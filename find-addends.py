#!/usr/bin/env python3

# find-addends.py

def findAddends(target, candidates, maxAddends=2):
    addends = []
    candidates.sort()

    if target in candidates:
        addends.append([target])

    # Remove candidates larger than the target
    while candidates[-1] >= target:
        x = candidates.pop()

    # For 2 addends
    for i in range(len(candidates) - 1):
        a = []
        for j in range(i + 1, len(candidates)):
            if candidates[i] + candidates[j] == target:
                a.append(candidates[i])
                a.append(candidates[j])
                addends.append(a)
                break
    
    return addends
